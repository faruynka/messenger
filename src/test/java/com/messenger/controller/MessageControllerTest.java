package com.messenger.controller;

import com.messenger.domain.Message;
import com.messenger.dto.MessageDto;
import com.messenger.service.MessageService;
import com.messenger.transformer.ITransformer;
import com.messenger.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MessageController.class)
class MessageControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private MessageController controller;

    @MockBean
    private MessageService messageService;
    @MockBean
    private ITransformer<Message, MessageDto> messageTransformer;

    @Test
    void getTest() throws Exception {
        given(messageService.find(TestUtils.MESSAGE1.getId()))
                .willReturn(TestUtils.MESSAGE1);
        given(messageTransformer.parseToDto(TestUtils.MESSAGE1))
                .willReturn(TestUtils.MESSAGE1_DTO);
        mvc.perform(get("/section/topic/message/{message_id}", TestUtils.MESSAGE1.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MESSAGE1_DTO.getId()))
                .andExpect(jsonPath("$.text", is(TestUtils.MESSAGE1_DTO.getText())));
    }

    @Test
    void getByTopicTest() throws Exception {
        given(messageService.findByTopic(TestUtils.MESSAGE1.getTopicId()))
                .willReturn(TestUtils.MESSAGE1);
        given(messageTransformer.parseToDto(TestUtils.MESSAGE1))
                .willReturn(TestUtils.MESSAGE1_DTO);
        mvc.perform(get("/section/topic/message/by_topic")
                .param("topic_id", TestUtils.MESSAGE1.getTopicId().toString()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MESSAGE1_DTO.getId()))
                .andExpect(jsonPath("$.text", is(TestUtils.MESSAGE1_DTO.getText())));
    }

    @Test
    void addTest() throws Exception {
        String messageDto = "{\"id\": \"" + TestUtils.MESSAGE1_DTO.getId() + "\"," +
                " \"text\" : \"" + TestUtils.MESSAGE1_DTO.getText() + "\"}";

        given(messageTransformer.parseToEntity(TestUtils.MESSAGE1_DTO)).willReturn(TestUtils.MESSAGE1);
        given(messageService.add(TestUtils.MESSAGE1.getUserId(), TestUtils.MESSAGE1.getTopicId(), TestUtils.MESSAGE1))
                .willReturn(TestUtils.MESSAGE1);
        given(messageTransformer.parseToDto(TestUtils.MESSAGE1)).willReturn(TestUtils.MESSAGE1_DTO);

        mvc.perform(post("/section/topic/message/add")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MESSAGE1.getUserId())
                .param("topic_id", TestUtils.MESSAGE1.getTopicId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(messageDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MESSAGE1_DTO.getId()))
                .andExpect(jsonPath("$.text", is(TestUtils.MESSAGE1_DTO.getText())));
    }

    @Test
    void editTest() throws Exception {
        String messageDto = "{\"id\": \"" + TestUtils.MESSAGE1_DTO.getId() + "\"," +
                " \"text\" : \"" + TestUtils.MESSAGE1_DTO.getText() + "\"}";

        given(messageTransformer.parseToEntity(TestUtils.MESSAGE1_DTO)).willReturn(TestUtils.MESSAGE1);
        given(messageService.edit(TestUtils.MESSAGE1.getUserId(), TestUtils.MESSAGE1))
                .willReturn(TestUtils.MESSAGE1);
        given(messageTransformer.parseToDto(TestUtils.MESSAGE1)).willReturn(TestUtils.MESSAGE1_DTO);

        mvc.perform(put("/section/topic/message/edit")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MESSAGE1.getUserId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(messageDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MESSAGE1_DTO.getId()))
                .andExpect(jsonPath("$.text", is(TestUtils.MESSAGE1_DTO.getText())));
    }

    @Test
    void removeTest() throws Exception {
        String messageDto = "{\"id\": \"" + TestUtils.MESSAGE1_DTO.getId() + "\"," +
                " \"text\" : \"" + TestUtils.MESSAGE1_DTO.getText() + "\"}";

        given(messageTransformer.parseToEntity(TestUtils.MESSAGE1_DTO)).willReturn(TestUtils.MESSAGE1);
        given(messageService.remove(TestUtils.MESSAGE1.getUserId(), TestUtils.MESSAGE1))
                .willReturn(TestUtils.MESSAGE1);
        given(messageTransformer.parseToDto(TestUtils.MESSAGE1)).willReturn(TestUtils.MESSAGE1_DTO);

        mvc.perform(delete("/section/topic/message/remove")
                .header(HttpHeaders.AUTHORIZATION, TestUtils.MESSAGE1.getUserId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(messageDto))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(TestUtils.MESSAGE1_DTO.getId()))
                .andExpect(jsonPath("$.text", is(TestUtils.MESSAGE1_DTO.getText())));
    }
}