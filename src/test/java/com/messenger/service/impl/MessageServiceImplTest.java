package com.messenger.service.impl;

import com.messenger.domain.Message;
import com.messenger.repo.MessageRepo;
import com.messenger.service.BusinessLogicMessageService;
import com.messenger.service.MessageService;
import com.messenger.service.PermissionService;
import com.messenger.utils.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.notNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MessageServiceImpl.class})
class MessageServiceImplTest {
    @Autowired
    MessageService messageService;

    @MockBean
    private MessageRepo messageRepo;
    @MockBean
    private BusinessLogicMessageService businessLogicMessageService;
    @MockBean
    private PermissionService permissionService;

    @Test
    void findTest() {
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
        .thenReturn(Optional.of(TestUtils.MESSAGE1));
        assertEquals(TestUtils.MESSAGE1, messageService.find(TestUtils.MESSAGE1.getId()));
    }

    @Test
    void findTestEntityNotFound() {
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
            ()->messageService.find(TestUtils.MESSAGE1.getId()));
    }

    @Test
    void findByTopicTest() {
        Mockito.when(messageRepo.findActiveByTopic(TestUtils.MESSAGE1.getTopicId()))
                .thenReturn(Optional.of(TestUtils.MESSAGE1));
        assertEquals(TestUtils.MESSAGE1, messageService.findByTopic(TestUtils.MESSAGE1.getTopicId()));
    }

    @Test
    void findByTopicTestEntityNotFound() {
        Mockito.when(messageRepo.findActiveByTopic(TestUtils.MESSAGE1.getTopicId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->messageService.findByTopic(TestUtils.MESSAGE1.getTopicId()));
    }

    @Test
    void addTest1() {
        Mockito.when(messageRepo.save(TestUtils.MESSAGE1))
                .thenReturn(TestUtils.MESSAGE1);
        assertEquals(TestUtils.MESSAGE1, messageService.add(TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(), TestUtils.MESSAGE1));
    }

    @Test
    void addTest2() {
        Mockito.when(messageRepo.save(TestUtils.MESSAGE1))
                .thenReturn(TestUtils.MESSAGE1);
        assertEquals(TestUtils.MESSAGE1, messageService.add(TestUtils.MESSAGE1));
    }

    @Test
    void addTest3() {
        Mockito.when(messageRepo.save(notNull()))
                .thenReturn(TestUtils.MESSAGE1);
        assertEquals(TestUtils.MESSAGE1, messageService.add(TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(), TestUtils.MESSAGE1.getText()));
    }

    @Test
    void editTestSelf() throws AccessDeniedException {
        Message oldMessage = new Message(
                TestUtils.MESSAGE1.getId(),
                TestUtils.MESSAGE1.getText() + "some more text",
                TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(),
                TestUtils.MESSAGE1.getCreatedDate(),
                TestUtils.MESSAGE1.getUpdatedDate(),
                TestUtils.MESSAGE1.getIsDeleted());
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
                .thenReturn(Optional.of(oldMessage));
        Mockito.when(messageRepo.save(oldMessage)).thenReturn(oldMessage);
        assertEquals(TestUtils.MESSAGE1, messageService.edit(TestUtils.MESSAGE1.getUserId(), TestUtils.MESSAGE1));
    }

    @Test
    void editTestAdmin() throws AccessDeniedException {
        final Long idShift = 1L;
        Message oldMessage = new Message(
                TestUtils.MESSAGE1.getId(),
                TestUtils.MESSAGE1.getText() + "some more text",
                TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(),
                TestUtils.MESSAGE1.getCreatedDate(),
                TestUtils.MESSAGE1.getUpdatedDate(),
                TestUtils.MESSAGE1.getIsDeleted());
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
                .thenReturn(Optional.of(oldMessage));
        Mockito.doNothing().when(permissionService).checkAdminPermission(TestUtils.MESSAGE1.getId() + idShift);
        Mockito.when(messageRepo.save(oldMessage)).thenReturn(oldMessage);
        assertEquals(TestUtils.MESSAGE1, messageService.edit(TestUtils.MESSAGE1.getUserId() + idShift,
                TestUtils.MESSAGE1));
    }

    @Test
    void editTestAccessDenied() throws AccessDeniedException {
        final Long idShift = 1L;
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
                .thenReturn(Optional.of(TestUtils.MESSAGE1));
        Mockito.doThrow(AccessDeniedException.class).when(permissionService)
                .checkAdminPermission(TestUtils.MESSAGE1.getId() + idShift);
        Assertions.assertThrows(AccessDeniedException.class,
                ()->messageService.edit(TestUtils.MESSAGE1.getUserId() + idShift, TestUtils.MESSAGE1));
    }

    @Test
    void editTestEntityNotFound() {
        final Long idShift = 1L;
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->messageService.edit(TestUtils.MESSAGE1.getUserId() + idShift, TestUtils.MESSAGE1));
    }

    @Test
    void removeTestNoUser() throws AccessDeniedException {
        Message message = new Message(
                TestUtils.MESSAGE1.getId(),
                TestUtils.MESSAGE1.getText(),
                TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(),
                TestUtils.MESSAGE1.getCreatedDate(),
                TestUtils.MESSAGE1.getUpdatedDate(),
                TestUtils.MESSAGE1.getIsDeleted());
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(message.getId()))
                .thenReturn(Optional.of(message));
        assertTrue(messageService.remove(message).getIsDeleted());
    }

    @Test
    void removeTestSelf() throws AccessDeniedException {
        Message message = new Message(
                TestUtils.MESSAGE1.getId(),
                TestUtils.MESSAGE1.getText(),
                TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(),
                TestUtils.MESSAGE1.getCreatedDate(),
                TestUtils.MESSAGE1.getUpdatedDate(),
                TestUtils.MESSAGE1.getIsDeleted());
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(message.getId()))
                .thenReturn(Optional.of(message));
        Mockito.doNothing().when(businessLogicMessageService).recursiveRemove(message.getUserId(),
                message);
        assertTrue(messageService.remove(message.getUserId(), message).getIsDeleted());
    }

    @Test
    void removeTestAdmin() throws AccessDeniedException {
        final Long idShift = 1L;
        Message message = new Message(
                TestUtils.MESSAGE1.getId(),
                TestUtils.MESSAGE1.getText(),
                TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(),
                TestUtils.MESSAGE1.getCreatedDate(),
                TestUtils.MESSAGE1.getUpdatedDate(),
                TestUtils.MESSAGE1.getIsDeleted());
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(message.getId()))
                .thenReturn(Optional.of(message));
        Mockito.doNothing().when(permissionService)
                .checkAdminPermission(TestUtils.MESSAGE1.getUserId() + idShift);
        Mockito.doNothing().when(businessLogicMessageService).recursiveRemove(message.getUserId(),
                message);
        assertTrue(messageService.remove(message.getUserId() + idShift, message).getIsDeleted());
    }

    @Test
    void removeTestEntityNotFound() {
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(TestUtils.MESSAGE1.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(EntityNotFoundException.class,
                ()->messageService.remove(TestUtils.MESSAGE1.getUserId(), TestUtils.MESSAGE1));
    }

    @Test
    void removeTestAccessDenied() throws AccessDeniedException {
        final Long idShift = 1L;
        Message message = new Message(
                TestUtils.MESSAGE1.getId(),
                TestUtils.MESSAGE1.getText(),
                TestUtils.MESSAGE1.getUserId(),
                TestUtils.MESSAGE1.getTopicId(),
                TestUtils.MESSAGE1.getCreatedDate(),
                TestUtils.MESSAGE1.getUpdatedDate(),
                TestUtils.MESSAGE1.getIsDeleted());
        Mockito.when(messageRepo.findByIdAndIsDeletedFalse(message.getId()))
                .thenReturn(Optional.of(message));
        Mockito.doThrow(AccessDeniedException.class).when(permissionService)
                .checkAdminPermission(TestUtils.MESSAGE1.getUserId() + idShift);

        Assertions.assertThrows(AccessDeniedException.class,
                ()->messageService.remove(TestUtils.MESSAGE1.getUserId() + idShift, TestUtils.MESSAGE1));
    }
}