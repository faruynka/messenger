package com.messenger.utils;

import com.messenger.domain.Message;
import com.messenger.dto.MessageDto;

import java.time.LocalDateTime;
import java.util.Date;

public class TestUtils {

    public static final Message MESSAGE1;
    public static final MessageDto MESSAGE1_DTO;

    static {
        MESSAGE1 = new Message(
                1L,
                "my text 1",
                1L,
                1L,
                LocalDateTime.now(),
                new Date(),
                false);
        MESSAGE1_DTO = new MessageDto(MESSAGE1.getId(), MESSAGE1.getText());
    }
}
