create sequence comment_seq start 1 increment 1;
create sequence message_seq start 1 increment 1;
create sequence section_seq start 1 increment 1;
create sequence topic_seq start 1 increment 1;
create table comment (
	id int8 not null,
	text varchar(2048),
	user_id int8 not null,
	message_id int8 not null,
	created_date timestamp,
	updated_at timestamp,
	is_deleted boolean,
	primary key (id)
);

create table message (
	id int8 not null,
	text varchar(2048),
	user_id int8 not null,
	topic_id int8 not null,
	created_date timestamp,
	updated_at timestamp,
	is_deleted boolean,
	primary key (id),
);

create table section (
	id int8 not null,
	name varchar(500) not null,
	user_id int8 not null,
	created_date timestamp,
	is_deleted boolean,
	primary key (id)
);

create table topic (
	id int8 not null,
	name varchar(500) not null,
	section_id int8 not null,
	message_id int8,
	user_id int8,
	is_deleted boolean,
	primary key (id)
);


alter table comment
  add constraint comment_message_id_fk foreign key(message_id) references message;
alter table message
  add constraint message_topic_id_fk foreign key(topic_id) references topic;
alter table topic
  add constraint topic_section_id_fk foreign key(section_id)references section;
alter table topic
  add constraint topic_message_id_fk foreign key(message_id)references message;