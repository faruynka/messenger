package com.messenger.service;

import com.messenger.domain.Message;
import com.messenger.domain.Topic;

import java.nio.file.AccessDeniedException;

public interface BusinessLogicTopicService {
    void recursiveRemove(Topic topic) throws AccessDeniedException;

    void recursiveForceRemove(Topic topic);

    Message addMessage(Long userId, Long topicId, String messageText);
}
