package com.messenger.service;

import com.messenger.domain.Message;
import com.messenger.domain.Topic;

import java.nio.file.AccessDeniedException;

public interface BusinessLogicMessageService {
    void recursiveRemove(Long userId, Message message) throws AccessDeniedException;
}
