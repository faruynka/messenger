package com.messenger.service.impl;

import com.messenger.domain.Comment;
import com.messenger.repo.CommentRepo;
import com.messenger.service.CommentService;
import com.messenger.service.PermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    private final CommentRepo commentRepo;
    private final PermissionService permissionService;

    public CommentServiceImpl(CommentRepo commentRepo, PermissionService permissionService) {
        this.commentRepo = commentRepo;
        this.permissionService = permissionService;
    }

    @Override
    @Transactional
    public Comment find(Long id) {
        return findImpl(id);
    }

    @Override
    @Transactional
    public List<Comment> findByMessage(Long messageId) {
        return commentRepo.findActiveByMessage(messageId);
    }

    @Override
    @Transactional
    public Comment add(Long userId, Long messageId, Comment comment) {
        comment.setUserId(userId);
        comment.setMessageId(messageId);
        return commentRepo.save(comment);
    }

    @Override
    @Transactional
    public Comment edit(Long userId, Comment comment) throws AccessDeniedException {
        Comment commentFromDb = findImpl(comment.getId());
        checkPermission(userId, commentFromDb.getId());
        commentFromDb.setText(comment.getText());
        return commentRepo.save(commentFromDb);
    }

    @Override
    @Transactional
    public Comment remove(Long userId, Comment comment) throws AccessDeniedException {
        Comment commentFromDb = findImpl(comment.getId());
        if (userId != null) {
            checkPermission(userId, commentFromDb.getId());
        }
        commentFromDb.setDeleted(true);
        return commentRepo.save(commentFromDb);
    }

    @Override
    public void forseRemove(Comment comment) {
        commentRepo.delete(comment);
    }

    private void checkPermission(Long currentUserId, Long userId) throws AccessDeniedException {
        if (!currentUserId.equals(userId)) {
            permissionService.checkAdminPermission(currentUserId);
        }
    }

    private Comment findImpl(Long id) {
        return commentRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() ->
                new EntityNotFoundException("there are no comment with such id = " + id));
    }
}
