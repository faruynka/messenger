package com.messenger.service.impl;

import com.messenger.domain.Section;
import com.messenger.domain.Topic;
import com.messenger.exception.BadRequestException;
import com.messenger.repo.SectionRepo;
import com.messenger.service.PermissionService;
import com.messenger.service.SectionService;
import com.messenger.service.TopicService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class SectionServiceImpl implements SectionService {
    private final SectionRepo sectionRepo;
    private final PermissionService permissionService;
    private final TopicService topicService;

    public SectionServiceImpl(SectionRepo sectionRepo, PermissionService permissionService, TopicService topicService) {
        this.sectionRepo = sectionRepo;
        this.permissionService = permissionService;
        this.topicService = topicService;
    }

    @Override
    @Transactional
    public Section find(Long sectionId) {
        return findImpl(sectionId);
    }

    @Override
    @Transactional
    public Iterable<Section> findAll() {
        return sectionRepo.findAll();
    }

    @Override
    @Transactional
    public List<Section> findActive() {
        return sectionRepo.findAllByIsDeletedIsFalse();
    }

    @Override
    @Transactional
    public Section add(Long userId, Section section) throws AccessDeniedException {
        Section sectionFromDb = sectionRepo.findByName(section.getName());
        if (sectionFromDb != null && sectionFromDb.getDeleted().equals(false)) {
            throw new BadRequestException("section name exists");
        }
        permissionService.checkAdminPermission(userId);
        section.setUserId(userId);
        return sectionRepo.save(section);
    }

    @Override
    @Transactional
    public Section edit(Long userId, Section section) throws AccessDeniedException {
        permissionService.checkAdminPermission(userId);
        Section sectionToChange = findImpl(section.getId());
        sectionToChange.setName(section.getName());
        return sectionRepo.save(sectionToChange);
    }

    @Override
    @Transactional
    public Section remove(Long userId, Section section) throws AccessDeniedException {
        permissionService.checkAdminPermission(userId);
        Section sectionToChange = findImpl(section.getId());
        for (Topic topic : topicService.findBySection(sectionToChange.getId())) {
            if (topic.getDeleted()) {
                topicService.remove(userId, topic);
            }
        }
        sectionToChange.setDeleted(true);
        return sectionRepo.save(sectionToChange);
    }

    @Override
    @Transactional
    public void forceRemove(Section section) {
        Section sectionToChange = findImpl(section.getId());
        for (Topic topic : topicService.findBySection(sectionToChange.getId())) {
            if (topic.getDeleted()) {
                topicService.forseRemove(topic);
            }
        }
        sectionRepo.delete(sectionToChange);
    }

    public Section findImpl(Long sectionId) {
        return sectionRepo.findByIdAndIsDeletedFalse(sectionId).orElseThrow(() ->
                new EntityNotFoundException("there are no section with such id = " + sectionId));
    }
}
