package com.messenger.service.impl;

import com.messenger.domain.Message;
import com.messenger.domain.Topic;
import com.messenger.repo.TopicRepo;
import com.messenger.service.BusinessLogicTopicService;
import com.messenger.service.PermissionService;
import com.messenger.service.TopicService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class TopicServiceImpl implements TopicService {
    private final TopicRepo topicRepo;
    private final BusinessLogicTopicService businessLogicTopicService;
    private final PermissionService permissionService;

    public TopicServiceImpl(TopicRepo topicRepo, BusinessLogicTopicService businessLogicTopicService,
                            PermissionService permissionService) {
        this.topicRepo = topicRepo;
        this.businessLogicTopicService = businessLogicTopicService;
        this.permissionService = permissionService;
    }

    @Override
    @Transactional
    public Topic find(Long id) {
        return findImpl(id);
    }

    @Override
    @Transactional
    public List<Topic> findBySection(Long sectionId) {
        return topicRepo.findActiveBySection(sectionId);
    }

    @Override
    @Transactional
    public Topic add(Long userId, Long sectionId, String topicName, String messageText) {
        Topic topic = new Topic();
        topic.setUserId(userId);
        topic.setSectionId(sectionId);
        topic.setName(topicName);
        Topic savedTopic = topicRepo.save(topic);
        Message addedMessage = businessLogicTopicService.addMessage(userId, topic.getId(), messageText);
        topic.setMessageId(addedMessage.getId());
        return topicRepo.save(savedTopic);
    }

    @Override
    @Transactional
    public Topic edit(Long userId, Topic topic) throws AccessDeniedException {
        checkPermission(userId, topic.getUserId());
        topic.setName(topic.getName());
        Topic topicToChange = findImpl(topic.getId());
        topicToChange.setName(topic.getName());
        return topicRepo.save(topicToChange);
    }

    @Override
    @Transactional
    public Topic remove(Long userId, Topic topic) throws AccessDeniedException {
        Topic topicToChange = findImpl(topic.getId());
        if (userId != null) {
            checkPermission(userId, topic.getUserId());
            businessLogicTopicService.recursiveRemove(topicToChange);
        }
        topicToChange.setDeleted(true);
        return topicRepo.save(topicToChange);
    }

    @Override
    public void forseRemove(Topic topic) {
        businessLogicTopicService.recursiveForceRemove(topic);
        topicRepo.delete(topic);
    }

    private void checkPermission(Long currentUserId, Long userId) throws AccessDeniedException {
        if (!currentUserId.equals(userId)) {
            permissionService.checkAdminPermission(currentUserId);
        }
    }

    private Topic findImpl(Long id) {
        return topicRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() ->
                new EntityNotFoundException("there are no topic with such id = " + id));
    }
}
