package com.messenger.service.impl;

import com.messenger.client.UserManagerClient;
import com.messenger.domain.Role;
import com.messenger.service.PermissionService;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;

@Service
public class PermissionServiceImpl implements PermissionService {
    private final UserManagerClient userManagerClient;

    public PermissionServiceImpl(UserManagerClient userManagerClient) {
        this.userManagerClient = userManagerClient;
    }

    @Override
    public void checkAdminPermission(Long userId) throws AccessDeniedException {
        if (!userManagerClient.get(userId).getRole().equals(Role.ADMIN)) {
            throw new AccessDeniedException("permission is denied");
        }
    }
}
