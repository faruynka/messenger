package com.messenger.service.impl;

import com.messenger.domain.Message;
import com.messenger.repo.MessageRepo;
import com.messenger.service.BusinessLogicMessageService;
import com.messenger.service.MessageService;
import com.messenger.service.PermissionService;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.nio.file.AccessDeniedException;

@Service
public class MessageServiceImpl implements MessageService {
    private final MessageRepo messageRepo;
    private final BusinessLogicMessageService businessLogicMessageService;
    private final PermissionService permissionService;

    public MessageServiceImpl(MessageRepo messageRepo, @Lazy BusinessLogicMessageService businessLogicMessageService,
                              PermissionService permissionService) {
        this.messageRepo = messageRepo;
        this.businessLogicMessageService = businessLogicMessageService;
        this.permissionService = permissionService;
    }

    @Override
    @Transactional
    public Message find(Long id) {
        return findImpl(id);
    }

    @Override
    @Transactional
    public Message findByTopic(Long topicId) {
        return messageRepo.findActiveByTopic(topicId).orElseThrow(() ->
                new EntityNotFoundException("there are no message with such topic id = " + topicId));
    }

    @Override
    @Transactional
    public Message add(Long userId, Long topicId, Message message) {
        message.setUserId(userId);
        message.setTopicId(topicId);
        return messageRepo.save(message);
    }

    @Override
    @Transactional
    public Message add(Message message) {
        return messageRepo.save(message);
    }

    @Override
    @Transactional
    public Message add(Long userId, Long topicId, String messageText) {
        Message message = new Message();
        message.setUserId(userId);
        message.setTopicId(topicId);
        message.setText(messageText);
        return messageRepo.save(message);
    }

    @Override
    @Transactional
    public Message edit(Long userId, Message message) throws AccessDeniedException {
        Message messageFromDB = findImpl(message.getId());
        checkPermission(userId, messageFromDB.getId());
        messageFromDB.setText(message.getText());
        return messageRepo.save(messageFromDB);
    }

    @Override
    @Transactional
    public Message remove(Long userId, Message message) throws AccessDeniedException {
        Message messageFromDB = findImpl(message.getId());
        if (userId != null) {
            checkPermission(userId, messageFromDB.getId());
            businessLogicMessageService.recursiveRemove(userId, messageFromDB);
        }
        messageFromDB.setIsDeleted(true);
        return messageFromDB;
    }

    @Override
    public void forceRemove(Message message) {
        messageRepo.delete(message);
    }

    private void checkPermission(Long currentUserId, Long userId) throws AccessDeniedException {
        if (!currentUserId.equals(userId)) {
            permissionService.checkAdminPermission(currentUserId);
        }
    }

    private Message findImpl(Long id) {
        return messageRepo.findByIdAndIsDeletedFalse(id).orElseThrow(() ->
                new EntityNotFoundException("there are no message with such id = " + id));
    }
}
