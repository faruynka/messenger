package com.messenger.service.impl;

import com.messenger.domain.Comment;
import com.messenger.domain.Message;
import com.messenger.domain.Topic;
import com.messenger.service.BusinessLogicTopicService;
import com.messenger.service.CommentService;
import com.messenger.service.MessageService;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class BusinessLogicTopicServiceImpl implements BusinessLogicTopicService {
    private final MessageService messageService;
    private final CommentService commentService;

    public BusinessLogicTopicServiceImpl(MessageService messageService, CommentService commentService) {
        this.messageService = messageService;
        this.commentService = commentService;
    }

    @Override
    public void recursiveRemove(Topic topic) throws AccessDeniedException {
        Message message = messageService.findByTopic(topic.getId());
        List<Comment> comments = commentService.findByMessage(message.getId());
        for (Comment comment: comments) {
            comment.setDeleted(true);
            commentService.remove(comment);
        }
        messageService.remove(message);
    }

    @Override
    public void recursiveForceRemove(Topic topic) {
        Message message = messageService.findByTopic(topic.getId());
        List<Comment> comments = commentService.findByMessage(message.getId());
        for (Comment comment: comments) {
            commentService.forseRemove(comment);
        }
        messageService.forceRemove(message);
    }

    @Override
    public Message addMessage(Long userId, Long topicId, String messageText) {
        return messageService.add(userId, topicId, messageText);
    }
}
