package com.messenger.service.impl;

import com.messenger.domain.Comment;
import com.messenger.domain.Message;
import com.messenger.domain.Topic;
import com.messenger.service.BusinessLogicMessageService;
import com.messenger.service.CommentService;
import com.messenger.service.TopicService;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class BusinessLogicMessageServiceImpl implements BusinessLogicMessageService {
    private final TopicService topicService;
    private final CommentService commentService;

    public BusinessLogicMessageServiceImpl(TopicService topicService, CommentService commentService) {
        this.topicService = topicService;
        this.commentService = commentService;
    }

    @Override
    public void recursiveRemove(Long userId, Message message) throws AccessDeniedException {
        Topic topic = topicService.find(message.getTopicId());
        List<Comment> comments = commentService.findByMessage(message.getId());
        for (Comment comment: comments) {
            comment.setDeleted(true);
            commentService.remove(comment);
        }
        topicService.remove(topic);
    }
}
