package com.messenger.service;

import com.messenger.domain.Comment;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface CommentService {

    Comment find(Long id);

    List<Comment> findByMessage(Long messageId);

    Comment add(Long userId, Long messageId, Comment comment);

    Comment edit(Long userId, Comment comment) throws AccessDeniedException;

    Comment remove(Long userId, Comment comment) throws AccessDeniedException;

    default Comment remove(Comment comment) throws AccessDeniedException {
        return remove(null, comment);
    }

    void forseRemove(Comment comment);
}
