package com.messenger.service;

import com.messenger.domain.Message;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface MessageService {

    Message find(Long id);

    Message findByTopic(Long topicId);

    Message add(Long userId, Long topicId, Message message);

    Message add(Message message);

    Message add(Long userId, Long topicId, String messageText);

    Message edit(Long userId, Message message) throws AccessDeniedException;

    Message remove(Long userId, Message message) throws AccessDeniedException;

    default Message remove(Message message) throws AccessDeniedException {
        return remove(null, message);
    }
    void forceRemove(Message message);
}
