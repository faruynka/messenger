package com.messenger.service;

import com.messenger.domain.Section;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface SectionService {

    Section find(Long sectionId);

    Section remove(Long userId, Section section) throws AccessDeniedException;

    Section add(Long userId, Section section) throws AccessDeniedException;

    Section edit(Long userId, Section section) throws AccessDeniedException;

    Iterable<Section> findAll();

    List<Section> findActive();

    void forceRemove(Section section);
}
