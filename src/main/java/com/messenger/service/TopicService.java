package com.messenger.service;

import com.messenger.domain.Topic;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface TopicService {
    Topic find(Long id);

    List<Topic> findBySection(Long sectionId);

    Topic add(Long userId, Long sectionId, String topicName, String messageText);

    Topic edit(Long userId, Topic topic) throws AccessDeniedException;

    Topic remove(Long userId, Topic topic) throws AccessDeniedException;

    default Topic remove(Topic topic) throws AccessDeniedException {
        return remove(null, topic);
    }

    void forseRemove(Topic topic);
}
