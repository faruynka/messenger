package com.messenger.service;

import java.nio.file.AccessDeniedException;

public interface PermissionService {
    void checkAdminPermission(Long userId) throws AccessDeniedException;
}
