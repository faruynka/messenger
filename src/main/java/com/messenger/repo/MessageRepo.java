package com.messenger.repo;

import com.messenger.domain.Message;
import com.messenger.domain.Topic;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface MessageRepo extends CrudRepository<Message, Long> {
    Long deleteByUserId(Long user);

    void deleteByIsDeletedTrue();

    List<Message> findAllByIsDeletedIsFalse();

    Message findByTopicId(Long topicId);

    @Query("select m from Message m where m.isDeleted = false and m.topicId = :topicId")
    Optional<Message> findActiveByTopic(@Param("topicId") Long topicId);

    @Modifying
    @Query("update Message m set m.isDeleted = true where m.topicId = :topicId")
    void setDeleteByTopic(@Param("topicId") Topic topicId);

    Optional<Message> findByIdAndIsDeletedFalse(Long id);
}


