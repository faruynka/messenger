package com.messenger.repo;

import com.messenger.domain.Comment;
import com.messenger.domain.Message;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CommentRepo extends CrudRepository<Comment, Long> {
    Long deleteByUserId(Long user);

    void deleteByIsDeletedTrue();

    List<Comment> findAllByIsDeletedIsFalse();

    List<Comment> findAllByMessageId(Message messageId);

    @Query("select c from Comment c where c.isDeleted = false and c.messageId = :messageId")
    List<Comment> findActiveByMessage(@Param("messageId") Long messageId);

    @Modifying
    @Query("update Comment c set c.isDeleted = true where c.messageId = :messageId")
    void setDeleteByMessage(@Param("messageId") Message messageId);

    Optional<Comment> findByIdAndIsDeletedFalse(Long aLong);
}


