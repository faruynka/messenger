package com.messenger.repo;

import com.messenger.domain.Section;
import com.messenger.domain.Topic;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface TopicRepo extends CrudRepository<Topic, Long> {

    List<Topic> findAllBySectionId(Long sectionId);

    @Query("select t from Topic t where t.isDeleted = false and t.sectionId = :sectionId")
    List<Topic> findActiveBySection(@Param("sectionId") Long sectionId);

    void deleteByIsDeleted(Boolean isDeleted);

    Optional<Topic> findByIdAndIsDeletedFalse(Long id);
}


