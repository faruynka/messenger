package com.messenger.repo;

import com.messenger.domain.Section;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SectionRepo extends CrudRepository<Section, Long> {

    List<Section> findAllByIsDeletedIsFalse();

    void deleteByIsDeleted(Boolean isDeleted);

    Optional<Section> findByIdAndIsDeletedFalse(Long id);

    Section findByName(String name);
}


