package com.messenger.client;

import com.messenger.dto.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "localhost:8082", name = "user")
public interface UserManagerClient {

    @GetMapping("/settings/user/get")
    UserDto get(@RequestParam Long userId);
}

