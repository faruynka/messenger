package com.messenger.domain;

public enum Role {
    USER,
    ADMIN
}
