package com.messenger.job;

import com.messenger.domain.Section;
import com.messenger.service.SectionService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DeleteTask {
    private final SectionService sectionService;

    public DeleteTask(SectionService sectionService) {
        this.sectionService = sectionService;
    }

    @Scheduled(cron = "0 0 12 */7 * ?")
    public void deleteMessages() {
        for (Section section : sectionService.findAll()) {
            if (section.getDeleted()) {
                sectionService.forceRemove(section);
            }
        }
    }
}
