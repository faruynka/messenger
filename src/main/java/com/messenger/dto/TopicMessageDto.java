package com.messenger.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class TopicMessageDto {

    private Long topicId;

    private Long messageId;

    @NotBlank(message = "Topic name is required")
    private String topicName;

    @NotBlank(message = "message is required")
    @Length(max = 2048, message = "message is too long")
    private String text;
}
