package com.messenger.dto;

import com.messenger.domain.Role;
import lombok.Data;

@Data
public class UserDto {
    private Long id;

    private String username;

    private String password;

    private String email;

    private Role role;
}
