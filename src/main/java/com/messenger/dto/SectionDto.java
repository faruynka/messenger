package com.messenger.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SectionDto {

    private Long id;

    @NotNull(message = "Section name is required")
    private String name;
}
