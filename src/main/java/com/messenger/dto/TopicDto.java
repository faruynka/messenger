package com.messenger.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TopicDto {

    private Long id;

    @NotBlank(message = "Topic name is required")
    private String name;

    private Long message;
}
