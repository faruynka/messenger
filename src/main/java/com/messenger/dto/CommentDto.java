package com.messenger.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class CommentDto implements Serializable {

    private Long id;

    @NotBlank(message = "message is required")
    @Length(max = 2048, message = "message is too long")
    private String text;
}
