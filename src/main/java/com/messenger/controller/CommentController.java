package com.messenger.controller;

import com.messenger.domain.Comment;
import com.messenger.dto.CommentDto;
import com.messenger.service.CommentService;
import com.messenger.transformer.ITransformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/section/topic/message/comment")
public class CommentController {
    private final CommentService commentService;
    private final ITransformer<Comment, CommentDto> commentTransformer;

    public CommentController(CommentService commentService, ITransformer<Comment, CommentDto> commentTransformer) {
        this.commentService = commentService;
        this.commentTransformer = commentTransformer;
    }

    @GetMapping("/{comment_id}")
    public CommentDto get(@PathVariable("comment_id") Long id) {
        return commentTransformer.parseToDto(commentService.find(id));
    }

    @GetMapping("/all")
    public List<CommentDto> getAllByMessage(@RequestParam("message_id") Long messageId) {
        return commentTransformer.parseToDto(commentService.findByMessage(messageId));
    }

    @PostMapping("/add")
    public CommentDto add(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                          @RequestParam("message_id") Long message_id, @Valid @RequestBody CommentDto commentDTO) {
        Comment comment = commentService.add(userId, message_id, commentTransformer.parseToEntity(commentDTO));
        return commentTransformer.parseToDto(comment);
    }

    @PutMapping("/edit")
    public CommentDto edit(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                           @Valid @RequestBody CommentDto commentDTO) throws AccessDeniedException {
        Comment comment = commentService.edit(userId, commentTransformer.parseToEntity(commentDTO));
        return commentTransformer.parseToDto(comment);
    }

    @DeleteMapping("/remove")
    public CommentDto remove(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                             @Valid @RequestBody CommentDto commentDTO) throws AccessDeniedException {
        Comment comment = commentService.remove(userId, commentTransformer.parseToEntity(commentDTO));
        return commentTransformer.parseToDto(comment);
    }
}
