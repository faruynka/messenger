package com.messenger.controller;

import com.messenger.client.UserManagerClient;
import com.messenger.domain.Section;
import com.messenger.dto.SectionDto;
import com.messenger.dto.UserDto;
import com.messenger.service.PermissionService;
import com.messenger.service.SectionService;
import com.messenger.transformer.ITransformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/section")
public class SectionController {
    private final SectionService sectionService;
    private final ITransformer<Section, SectionDto> sectionTransformer;
    private final UserManagerClient userManagerClient;

    public SectionController(SectionService sectionService, ITransformer<Section, SectionDto> sectionTransformer,
                             UserManagerClient userManagerClient) {
        this.sectionService = sectionService;
        this.sectionTransformer = sectionTransformer;
        this.userManagerClient = userManagerClient;
    }

    @GetMapping("/{section_id}")
    public SectionDto get(@PathVariable("section_id") Long id) {
        return sectionTransformer.parseToDto(sectionService.find(id));
    }

    @GetMapping("/all")
    public List<Section> getAllSections() {
        return sectionService.findActive();
    }

    @PostMapping("/add")
    public SectionDto add(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                          @Valid @RequestBody SectionDto sectionDto) throws AccessDeniedException {
        Section section = sectionService.add(userId, sectionTransformer.parseToEntity(sectionDto));
        return sectionTransformer.parseToDto(section);
    }

    @PutMapping("/edit")
    public SectionDto edit(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                           @Valid @RequestBody SectionDto sectionDto) throws AccessDeniedException {
        Section section = sectionService.edit(userId, sectionTransformer.parseToEntity(sectionDto));
        return sectionTransformer.parseToDto(section);
    }

    @DeleteMapping("/remove")
    public SectionDto remove(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                             @Valid @RequestBody SectionDto sectionDto) throws AccessDeniedException {
        Section section = sectionService.remove(userId, sectionTransformer.parseToEntity(sectionDto));
        return sectionTransformer.parseToDto(section);
    }
}
