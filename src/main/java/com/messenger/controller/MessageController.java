package com.messenger.controller;

import com.messenger.domain.Message;
import com.messenger.dto.MessageDto;
import com.messenger.service.MessageService;
import com.messenger.transformer.ITransformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("/section/topic/message")
public class MessageController {
    private final MessageService messageService;
    private final ITransformer<Message, MessageDto> messageTransformer;

    public MessageController(MessageService messageService, ITransformer<Message, MessageDto> messageTransformer) {
        this.messageService = messageService;
        this.messageTransformer = messageTransformer;
    }

    @GetMapping("/{message_id}")
    public MessageDto get(@PathVariable("message_id") Long id) {
        return messageTransformer.parseToDto(messageService.find(id));
    }

    @GetMapping("/by_topic")
    public MessageDto getByTopic(@RequestParam("topic_id") Long topic) {
        return messageTransformer.parseToDto(messageService.findByTopic(topic));
    }

    @PostMapping("/add")
    public MessageDto add(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                          @RequestParam("topic_id") Long topicId, @Valid @RequestBody MessageDto messageDTO) {
        Message message = messageService.add(userId, topicId, messageTransformer.parseToEntity(messageDTO));
        return messageTransformer.parseToDto(message);
    }

    @PutMapping("/edit")
    public MessageDto edit(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                           @Valid @RequestBody MessageDto messageDTO) throws AccessDeniedException {
        Message message = messageService.edit(userId, messageTransformer.parseToEntity(messageDTO));
        return messageTransformer.parseToDto(message);
    }

    @DeleteMapping("/remove")
    public MessageDto remove(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                             @Valid @RequestBody MessageDto messageDTO) throws AccessDeniedException {
        Message message = messageService.remove(userId, messageTransformer.parseToEntity(messageDTO));
        return messageTransformer.parseToDto(message);
    }
}
