package com.messenger.controller;

import com.messenger.domain.Message;
import com.messenger.domain.Topic;
import com.messenger.dto.MessageDto;
import com.messenger.dto.TopicDto;
import com.messenger.dto.TopicMessageDto;
import com.messenger.service.TopicService;
import com.messenger.transformer.ITransformer;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.AccessDeniedException;
import java.util.List;

@RestController
@RequestMapping("section/topic")
public class TopicController {
    private final TopicService topicService;
    private final ITransformer<Topic, TopicDto> topicTransformer;
    private final ITransformer<Message, MessageDto> messageTransformer;

    public TopicController(TopicService topicService, ITransformer<Topic, TopicDto> topicTransformer,
                           ITransformer<Message, MessageDto> messageTransformer) {
        this.topicService = topicService;
        this.topicTransformer = topicTransformer;
        this.messageTransformer = messageTransformer;
    }

    @GetMapping("/{topic_id}")
    public TopicDto get(@PathVariable("topic_id") Long id) {
        return topicTransformer.parseToDto(topicService.find(id));
    }

    @GetMapping("/all")
    public List<TopicDto> getAllBySection(@RequestParam("section_id") Long section) {
        return topicTransformer.parseToDto(topicService.findBySection(section));
    }

    @PostMapping("/add")
    public TopicDto add(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                        @RequestParam("section_id") Long section, @Valid @RequestBody TopicMessageDto topicMessageDto) {
        Topic topic = topicService.add(userId, section, topicMessageDto.getTopicName(), topicMessageDto.getText());
        return topicTransformer.parseToDto(topic);
    }

    @PutMapping("/edit")
    public TopicDto edit(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                         @Valid @RequestBody TopicDto topicDto) throws AccessDeniedException {
        Topic topic = topicService.edit(userId, topicTransformer.parseToEntity(topicDto));
        return topicTransformer.parseToDto(topic);
    }

    @DeleteMapping("/remove")
    public TopicDto remove(@RequestHeader(HttpHeaders.AUTHORIZATION) Long userId,
                           @Valid @RequestBody TopicDto topicDto) throws AccessDeniedException {
        Topic topic = topicService.remove(userId, topicTransformer.parseToEntity(topicDto));
        return topicTransformer.parseToDto(topic);
    }
}