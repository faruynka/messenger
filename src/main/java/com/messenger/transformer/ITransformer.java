package com.messenger.transformer;

import java.util.List;
import java.util.stream.Collectors;

public interface ITransformer<T, G> {

    T parseToEntity(T entity, G dto);

    default T parseToEntity(G dto) {
        return parseToEntity(null, dto);
    }

    G parseToDto(T entity);

    default List<G> parseToDto(List<T> entities) {
        return entities.stream().map(this::parseToDto).collect(Collectors.toList());
    }
}
