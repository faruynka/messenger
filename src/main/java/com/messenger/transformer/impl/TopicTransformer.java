package com.messenger.transformer.impl;

import com.messenger.domain.Topic;
import com.messenger.dto.TopicDto;
import com.messenger.transformer.ITransformer;
import org.springframework.stereotype.Component;

@Component
public class TopicTransformer implements ITransformer<Topic, TopicDto> {
    @Override
    public Topic parseToEntity(Topic topic, TopicDto topicDTO) {
        if (topicDTO == null) {
            return null;
        }
        if (topic == null) {
            topic = new Topic();
        }
        if (topicDTO.getId() != null) {
            topic.setId(topicDTO.getId());
        }
        if (topicDTO.getName() != null) {
            topic.setName(topicDTO.getName());
        }
        if (topicDTO.getMessage() != null) {
            topic.setMessageId(topicDTO.getMessage());
        }
        return topic;
    }

    @Override
    public TopicDto parseToDto(Topic topic) {
        if (topic == null) {
            return null;
        }
        TopicDto topicDTO = new TopicDto();
        topicDTO.setId(topic.getId());
        topicDTO.setName(topic.getName());
        topicDTO.setMessage(topic.getMessageId());
        return topicDTO;
    }
}
