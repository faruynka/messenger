package com.messenger.transformer.impl;

import com.messenger.domain.Section;
import com.messenger.dto.SectionDto;
import com.messenger.transformer.ITransformer;
import org.springframework.stereotype.Component;

@Component
public class SectionTransformer implements ITransformer<Section, SectionDto> {

    @Override
    public Section parseToEntity(Section section, SectionDto sectionDTO) {
        if (sectionDTO == null) {
            return null;
        }
        if (section == null) {
            section = new Section();
        }
        if (sectionDTO.getId() != null) {
            section.setId(sectionDTO.getId());
        }
        if (sectionDTO.getName() != null) {
            section.setName(sectionDTO.getName());
        }
        return section;
    }

    @Override
    public SectionDto parseToDto(Section section) {
        if (section == null) {
            return null;
        }
        SectionDto sectionDTO = new SectionDto();
        sectionDTO.setId(section.getId());
        sectionDTO.setName(section.getName());
        return sectionDTO;
    }
}
