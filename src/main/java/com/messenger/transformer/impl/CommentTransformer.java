package com.messenger.transformer.impl;

import com.messenger.domain.Comment;
import com.messenger.dto.CommentDto;
import com.messenger.transformer.ITransformer;
import org.springframework.stereotype.Component;

@Component
public class CommentTransformer implements ITransformer<Comment, CommentDto> {

    @Override
    public Comment parseToEntity(Comment comment, CommentDto commentDTO) {
        if (commentDTO == null) {
            return null;
        }
        if (comment == null) {
            comment = new Comment();
        }
        if (commentDTO.getId() != null) {
            comment.setId(commentDTO.getId());
        }
        if (commentDTO.getText() != null) {
            comment.setText(commentDTO.getText());
        }
        return comment;
    }

    @Override
    public CommentDto parseToDto(Comment comment) {
        if (comment == null) {
            return null;
        }
        CommentDto commentDTO = new CommentDto();
        commentDTO.setId(comment.getId());
        commentDTO.setText(comment.getText());
        return commentDTO;
    }
}
