package com.messenger.transformer.impl;

import com.messenger.dto.MessageDto;
import com.messenger.transformer.ITransformer;
import com.messenger.domain.Message;
import org.springframework.stereotype.Component;

@Component
public class MessageTransformer implements ITransformer<Message, MessageDto> {

    @Override
    public Message parseToEntity(Message message, MessageDto messageDTO) {
        if (messageDTO == null) {
            return null;
        }
        if (message == null) {
            message = new Message();
        }
        if (messageDTO.getId() != null) {
            message.setId(messageDTO.getId());
        }
        if (messageDTO.getText() != null) {
            message.setText(messageDTO.getText());
        }
        return message;
    }

    @Override
    public MessageDto parseToDto(Message message) {
        if (message == null) {
            return null;
        }
        MessageDto messageDTO = new MessageDto();
        messageDTO.setId(message.getId());
        messageDTO.setText(message.getText());
        return messageDTO;
    }
}
