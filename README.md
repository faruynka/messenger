# Description:

Messenger is a microservice which works with comments, messeges, topics, and sections. It works together with  UserManager microservice and they provide "forum" functionality. 
Section is the largest partition of the forum content. It contains topics, which consists of their message and comments. All this data can be added, removed, edited or recovered by users.
There are two types of users: user and administrator. Administrators can work with all messages, but users only with their own. And they all can add new topics, messages, and comments, 
but only administrators can make new sections.

# Technologies used:

* spring boot
* migrations-flyway
* microservices-feign client
* test-junit
* mockito
* db-postgress
* spring data jpa.
